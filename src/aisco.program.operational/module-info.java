module aisco.program.operational {
    requires aisco.program.core;
    exports aisco.program.operational;
    requires vmj.routing.route;
    requires prices.auth.vmj;
}

package aisco.product.bisakita;

import java.util.ArrayList;

import vmj.routing.route.VMJServer;
import vmj.routing.route.Router;
import vmj.hibernate.integrator.HibernateUtil;
import org.hibernate.cfg.Configuration;

import aisco.program.ProgramResourceFactory;
import aisco.program.core.ProgramResource;
import aisco.chartofaccount.ChartOfAccountResourceFactory;
import aisco.chartofaccount.core.ChartOfAccountResource;
import aisco.financialreport.FinancialReportResourceFactory;
import aisco.financialreport.core.FinancialReportResource;
import aisco.automaticreport.AutomaticReportResourceFactory;
import aisco.automaticreport.core.AutomaticReportResource;
import aisco.donation.DonationResourceFactory;
import aisco.donation.core.DonationResource;

import prices.auth.vmj.model.UserResourceFactory;
import prices.auth.vmj.model.RoleResourceFactory;
import prices.auth.vmj.model.core.UserResource;
import prices.auth.vmj.model.core.RoleResource;


public class BisaKita {

	public static void main(String[] args) {
		activateServer("localhost", 7776);
		Configuration configuration = new Configuration();
		configuration.addAnnotatedClass(aisco.program.core.Program.class);
		configuration.addAnnotatedClass(aisco.program.core.ProgramComponent.class);
		configuration.addAnnotatedClass(aisco.program.core.ProgramDecorator.class);
		configuration.addAnnotatedClass(aisco.program.activity.ProgramImpl.class);
		configuration.addAnnotatedClass(aisco.chartofaccount.core.ChartOfAccount.class);
		configuration.addAnnotatedClass(aisco.chartofaccount.core.ChartOfAccountComponent.class);
		configuration.addAnnotatedClass(aisco.chartofaccount.core.ChartOfAccountDecorator.class);
		configuration.addAnnotatedClass(aisco.chartofaccount.core.ChartOfAccountImpl.class);
		configuration.addAnnotatedClass(aisco.financialreport.core.FinancialReport.class);
		configuration.addAnnotatedClass(aisco.financialreport.core.FinancialReportComponent.class);
		configuration.addAnnotatedClass(aisco.financialreport.core.FinancialReportDecorator.class);
		configuration.addAnnotatedClass(aisco.financialreport.core.FinancialReportImpl.class);
		configuration.addAnnotatedClass(aisco.financialreport.income.FinancialReportImpl.class);
		configuration.addAnnotatedClass(aisco.financialreport.expense.FinancialReportImpl.class);
		configuration.addAnnotatedClass(aisco.automaticreport.periodic.AutomaticReportPeriodicImpl.class);
		configuration.addAnnotatedClass(aisco.donation.core.Donation.class);
		configuration.addAnnotatedClass(aisco.donation.core.DonationComponent.class);
		configuration.addAnnotatedClass(aisco.donation.core.DonationDecorator.class);
		configuration.addAnnotatedClass(aisco.donation.core.DonationImpl.class);
		configuration.addAnnotatedClass(aisco.donation.confirmation.DonationConfirmationDecorator.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.core.UserComponent.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.core.UserImpl.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.passworded.UserPasswordedImpl.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.core.RoleComponent.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.core.RoleImpl.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.core.UserRoleImpl.class);
		configuration.buildMappings();
		HibernateUtil.buildSessionFactory(configuration);

		createObjectsAndBindEndPoints();
	}

	public static void activateServer(String hostName, int portNumber) {
		VMJServer vmjServer = VMJServer.getInstance(hostName, portNumber);
		try {
			vmjServer.startServerGeneric();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public static void createObjectsAndBindEndPoints() {
		System.out.println("== CREATING OBJECTS AND BINDING ENDPOINTS ==");
		ProgramResource activity = ProgramResourceFactory
			.createProgramResource(
			"aisco.program.activity.ProgramResourceImpl"
			);
		ChartOfAccountResource chartofaccount = ChartOfAccountResourceFactory
			.createChartOfAccountResource(
			"aisco.chartofaccount.core.ChartOfAccountResourceImpl"
			);
		FinancialReportResource financialreport = FinancialReportResourceFactory
			.createFinancialReportResource(
			"aisco.financialreport.core.FinancialReportResourceImpl"
			);
		FinancialReportResource income = FinancialReportResourceFactory
			.createFinancialReportResource(
			"aisco.financialreport.income.FinancialReportResourceIncomeDecorator"
			,
			FinancialReportResourceFactory.createFinancialReportResource(
			"aisco.financialreport.core.FinancialReportResourceImpl"));
		FinancialReportResource expense = FinancialReportResourceFactory
			.createFinancialReportResource(
			"aisco.financialreport.expense.FinancialReportResourceExpenseDecorator"
			,
			FinancialReportResourceFactory.createFinancialReportResource(
			"aisco.financialreport.core.FinancialReportResourceImpl"));
		AutomaticReportResource automaticreport = AutomaticReportResourceFactory
			.createAutomaticReportResource(
			"aisco.automaticreport.core.AutomaticReportResourceImpl"
			);
		AutomaticReportResource periodic = AutomaticReportResourceFactory
			.createAutomaticReportResource(
			"aisco.automaticreport.periodic.AutomaticReportResourceImpl"
			,
			AutomaticReportResourceFactory.createAutomaticReportResource(
			"aisco.automaticreport.core.AutomaticReportResourceImpl"));
		DonationResource donation = DonationResourceFactory
			.createDonationResource(
			"aisco.donation.core.DonationResourceImpl"
			);
		DonationResource confirmation = DonationResourceFactory
			.createDonationResource(
			"aisco.donation.confirmation.DonationResourceConfirmationDecorator"
			,
			DonationResourceFactory.createDonationResource(
			"aisco.donation.core.DonationResourceImpl"));
		
		UserResource userCore = UserResourceFactory
				.createUserResource("prices.auth.vmj.model.core.UserResourceImpl");
		UserResource userPassworded = UserResourceFactory
				.createUserResource("prices.auth.vmj.model.passworded.UserPasswordedResourceDecorator", userCore);
		RoleResource roleCore = RoleResourceFactory
				.createRoleResource("prices.auth.vmj.model.core.RoleResourceImpl");


		System.out.println("automaticreport endpoints binding");
		Router.route(automaticreport);
		Router.route(periodic);

		System.out.println("confirmation endpoints binding");
		Router.route(confirmation);
		
		System.out.println("donation endpoints binding");
		Router.route(donation);
		
		System.out.println("expense endpoints binding");
		Router.route(expense);
		
		System.out.println("income endpoints binding");
		Router.route(income);
		
		System.out.println("financialreport endpoints binding");
		Router.route(financialreport);
		
		System.out.println("chartofaccount endpoints binding");
		Router.route(chartofaccount);
		
		System.out.println("activity endpoints binding");
		Router.route(activity);
		
		
		Router.route(userPassworded);
		Router.route(roleCore);
		System.out.println();
	}

}
package aisco.product.bahumembahu;

import java.util.ArrayList;

import vmj.routing.route.VMJServer;
import vmj.routing.route.Router;
import vmj.hibernate.integrator.HibernateUtil;
import org.hibernate.cfg.Configuration;

import volunteers.volunteer.VolunteerResourceFactory;
import volunteers.volunteer.core.VolunteerResource;
import volunteers.community.CommunityResourceFactory;
import volunteers.community.core.CommunityResource;
import aisco.program.ProgramResourceFactory;
import aisco.program.core.ProgramResource;
import aisco.chartofaccount.ChartOfAccountResourceFactory;
import aisco.chartofaccount.core.ChartOfAccountResource;
import aisco.financialreport.FinancialReportResourceFactory;
import aisco.financialreport.core.FinancialReportResource;

import prices.auth.vmj.model.UserResourceFactory;
import prices.auth.vmj.model.RoleResourceFactory;
import prices.auth.vmj.model.core.UserResource;
import prices.auth.vmj.model.core.RoleResource;


public class BahuMembahu {

	public static void main(String[] args) {
		activateServer("localhost", 7776);
		Configuration configuration = new Configuration();
		configuration.addAnnotatedClass(volunteers.volunteer.core.Volunteer.class);
		configuration.addAnnotatedClass(volunteers.volunteer.core.VolunteerComponent.class);
		configuration.addAnnotatedClass(volunteers.volunteer.core.VolunteerDecorator.class);
		configuration.addAnnotatedClass(volunteers.volunteer.core.VolunteerImpl.class);
		configuration.addAnnotatedClass(volunteers.community.core.Community.class);
		configuration.addAnnotatedClass(volunteers.community.core.CommunityComponent.class);
		configuration.addAnnotatedClass(volunteers.community.core.CommunityDecorator.class);
		configuration.addAnnotatedClass(volunteers.community.core.CommunityImpl.class);
		configuration.addAnnotatedClass(aisco.program.core.Program.class);
		configuration.addAnnotatedClass(aisco.program.core.ProgramComponent.class);
		configuration.addAnnotatedClass(aisco.program.core.ProgramDecorator.class);
		configuration.addAnnotatedClass(aisco.program.activity.ProgramImpl.class);
		configuration.addAnnotatedClass(aisco.program.withvolunteer.ProgramImpl.class);
		configuration.addAnnotatedClass(aisco.chartofaccount.core.ChartOfAccount.class);
		configuration.addAnnotatedClass(aisco.chartofaccount.core.ChartOfAccountComponent.class);
		configuration.addAnnotatedClass(aisco.chartofaccount.core.ChartOfAccountDecorator.class);
		configuration.addAnnotatedClass(aisco.chartofaccount.core.ChartOfAccountImpl.class);
		configuration.addAnnotatedClass(aisco.financialreport.core.FinancialReport.class);
		configuration.addAnnotatedClass(aisco.financialreport.core.FinancialReportComponent.class);
		configuration.addAnnotatedClass(aisco.financialreport.core.FinancialReportDecorator.class);
		configuration.addAnnotatedClass(aisco.financialreport.core.FinancialReportImpl.class);
		configuration.addAnnotatedClass(aisco.financialreport.income.FinancialReportImpl.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.core.UserComponent.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.core.UserImpl.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.passworded.UserPasswordedImpl.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.core.RoleComponent.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.core.RoleImpl.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.core.UserRoleImpl.class);
		configuration.buildMappings();
		HibernateUtil.buildSessionFactory(configuration);

		createObjectsAndBindEndPoints();
	}

	public static void activateServer(String hostName, int portNumber) {
		VMJServer vmjServer = VMJServer.getInstance(hostName, portNumber);
		try {
			vmjServer.startServerGeneric();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public static void createObjectsAndBindEndPoints() {
		System.out.println("== CREATING OBJECTS AND BINDING ENDPOINTS ==");
		VolunteerResource volunteer = VolunteerResourceFactory
			.createVolunteerResource(
			"volunteers.volunteer.core.VolunteerResourceImpl"
			);
		CommunityResource community = CommunityResourceFactory
			.createCommunityResource(
			"volunteers.community.core.CommunityResourceImpl"
			);
		ProgramResource program = ProgramResourceFactory
			.createProgramResource(
			"aisco.program.activity.ProgramResourceImpl"
			);
		ProgramResource withvolunteer = ProgramResourceFactory
			.createProgramResource(
			"aisco.program.withvolunteer.ProgramResourceImpl"
			,
			ProgramResourceFactory.createProgramResource(
			"aisco.program.core.ProgramResourceImpl"));
		ChartOfAccountResource chartofaccount = ChartOfAccountResourceFactory
			.createChartOfAccountResource(
			"aisco.chartofaccount.core.ChartOfAccountResourceImpl"
			);
		FinancialReportResource financialreport = FinancialReportResourceFactory
			.createFinancialReportResource(
			"aisco.financialreport.core.FinancialReportResourceImpl"
			);
		FinancialReportResource income = FinancialReportResourceFactory
			.createFinancialReportResource(
			"aisco.financialreport.income.FinancialReportResourceIncomeDecorator"
			,
			FinancialReportResourceFactory.createFinancialReportResource(
			"aisco.financialreport.core.FinancialReportResourceImpl"));
		
		UserResource userCore = UserResourceFactory
				.createUserResource("prices.auth.vmj.model.core.UserResourceImpl");
		UserResource userPassworded = UserResourceFactory
				.createUserResource("prices.auth.vmj.model.passworded.UserPasswordedResourceDecorator", userCore);
		RoleResource roleCore = RoleResourceFactory
				.createRoleResource("prices.auth.vmj.model.core.RoleResourceImpl");


		System.out.println("income endpoints binding");
		Router.route(income);
		
		System.out.println("financialreport endpoints binding");
		Router.route(financialreport);
		
		System.out.println("chartofaccount endpoints binding");
		Router.route(chartofaccount);
		
		System.out.println("withvolunteer endpoints binding");
		Router.route(withvolunteer);
		
		System.out.println("program endpoints binding");
		Router.route(program);
		
		System.out.println("community endpoints binding");
		Router.route(community);
		
		System.out.println("volunteer endpoints binding");
		Router.route(volunteer);
		
		
		Router.route(userPassworded);
		Router.route(roleCore);
		System.out.println();
	}

}
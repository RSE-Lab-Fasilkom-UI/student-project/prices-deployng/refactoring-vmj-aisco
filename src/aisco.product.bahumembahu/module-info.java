module aisco.product.bahumembahu {
    requires vmj.routing.route;
    requires vmj.hibernate.integrator;
    
    requires net.bytebuddy;
    requires java.xml.bind;
    requires com.sun.xml.bind;
    requires com.fasterxml.classmate;
    requires jdk.unsupported;

    requires volunteers.volunteer.core;
    requires volunteers.community.core;
    requires aisco.program.core;
    requires aisco.program.withvolunteer;
    requires aisco.chartofaccount.core;
    requires aisco.financialreport.core;
    requires aisco.financialreport.income;
	
	requires prices.auth.vmj;
    requires prices.auth.vmj.model;
}
package aisco.product.sheilaschool;

import java.util.ArrayList;

import vmj.routing.route.VMJServer;
import vmj.routing.route.Router;
import vmj.hibernate.integrator.HibernateUtil;
import org.hibernate.cfg.Configuration;

import aisco.program.ProgramResourceFactory;
import aisco.program.core.ProgramResource;
import aisco.chartofaccount.ChartOfAccountResourceFactory;
import aisco.chartofaccount.core.ChartOfAccountResource;
import aisco.financialreport.FinancialReportResourceFactory;
import aisco.financialreport.core.FinancialReportResource;
import aisco.automaticreport.AutomaticReportResourceFactory;
import aisco.automaticreport.core.AutomaticReportResource;
import aisco.summary.SummaryResourceFactory;
import aisco.summary.core.SummaryResource;

import prices.auth.vmj.model.UserResourceFactory;
import prices.auth.vmj.model.RoleResourceFactory;
import prices.auth.vmj.model.core.UserResource;
import prices.auth.vmj.model.core.RoleResource;


public class SheilaSchool {

	public static void main(String[] args) {
		activateServer("localhost", 7776);
		Configuration configuration = new Configuration();
		configuration.addAnnotatedClass(aisco.program.core.Program.class);
		configuration.addAnnotatedClass(aisco.program.core.ProgramComponent.class);
		configuration.addAnnotatedClass(aisco.program.core.ProgramDecorator.class);
		configuration.addAnnotatedClass(aisco.program.activity.ProgramImpl.class);
		configuration.addAnnotatedClass(aisco.chartofaccount.core.ChartOfAccount.class);
		configuration.addAnnotatedClass(aisco.chartofaccount.core.ChartOfAccountComponent.class);
		configuration.addAnnotatedClass(aisco.chartofaccount.core.ChartOfAccountDecorator.class);
		configuration.addAnnotatedClass(aisco.chartofaccount.core.ChartOfAccountImpl.class);
		configuration.addAnnotatedClass(aisco.financialreport.core.FinancialReport.class);
		configuration.addAnnotatedClass(aisco.financialreport.core.FinancialReportComponent.class);
		configuration.addAnnotatedClass(aisco.financialreport.core.FinancialReportDecorator.class);
		configuration.addAnnotatedClass(aisco.financialreport.core.FinancialReportImpl.class);
		configuration.addAnnotatedClass(aisco.financialreport.income.FinancialReportImpl.class);
		configuration.addAnnotatedClass(aisco.financialreport.expense.FinancialReportImpl.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.core.UserComponent.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.core.UserImpl.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.passworded.UserPasswordedImpl.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.core.RoleComponent.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.core.RoleImpl.class);
		configuration.addAnnotatedClass(prices.auth.vmj.model.core.UserRoleImpl.class);
		configuration.buildMappings();
		HibernateUtil.buildSessionFactory(configuration);

		createObjectsAndBindEndPoints();
	}

	public static void activateServer(String hostName, int portNumber) {
		VMJServer vmjServer = VMJServer.getInstance(hostName, portNumber);
		try {
			vmjServer.startServerGeneric();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public static void createObjectsAndBindEndPoints() {
		System.out.println("== CREATING OBJECTS AND BINDING ENDPOINTS ==");
		ProgramResource activity = ProgramResourceFactory
			.createProgramResource(
			"aisco.program.activity.ProgramResourceImpl"
			);
		ChartOfAccountResource chartofaccount = ChartOfAccountResourceFactory
			.createChartOfAccountResource(
			"aisco.chartofaccount.core.ChartOfAccountResourceImpl"
			);
		FinancialReportResource financialreport = FinancialReportResourceFactory
			.createFinancialReportResource(
			"aisco.financialreport.core.FinancialReportResourceImpl"
			);
		FinancialReportResource income = FinancialReportResourceFactory
			.createFinancialReportResource(
			"aisco.financialreport.income.FinancialReportResourceIncomeDecorator"
			,
			FinancialReportResourceFactory.createFinancialReportResource(
			"aisco.financialreport.core.FinancialReportResourceImpl"));
		FinancialReportResource expense = FinancialReportResourceFactory
			.createFinancialReportResource(
			"aisco.financialreport.expense.FinancialReportResourceExpenseDecorator"
			,
			FinancialReportResourceFactory.createFinancialReportResource(
			"aisco.financialreport.core.FinancialReportResourceImpl"));
		AutomaticReportResource automaticreport = AutomaticReportResourceFactory
			.createAutomaticReportResource(
			"aisco.automaticreport.core.AutomaticReportResourceImpl"
			);
		AutomaticReportResource twolevel = AutomaticReportResourceFactory
			.createAutomaticReportResource(
			"aisco.automaticreport.twolevel.AutomaticReportResourceImpl"
			,
			AutomaticReportResourceFactory.createAutomaticReportResource(
			"aisco.automaticreport.core.AutomaticReportResourceImpl"));
		SummaryResource summary = SummaryResourceFactory
			.createSummaryResource(
			"aisco.summary.core.SummaryResourceImpl"
			);
		
		UserResource userCore = UserResourceFactory
				.createUserResource("prices.auth.vmj.model.core.UserResourceImpl");
		UserResource userPassworded = UserResourceFactory
				.createUserResource("prices.auth.vmj.model.passworded.UserPasswordedResourceDecorator", userCore);
		RoleResource roleCore = RoleResourceFactory
				.createRoleResource("prices.auth.vmj.model.core.RoleResourceImpl");


		System.out.println("summary endpoints binding");
		Router.route(summary);
		
		System.out.println("twolevel endpoints binding");
		Router.route(twolevel);
		
		System.out.println("automaticreport endpoints binding");
		Router.route(automaticreport);
		
		System.out.println("expense endpoints binding");
		Router.route(expense);
		
		System.out.println("income endpoints binding");
		Router.route(income);
		
		System.out.println("financialreport endpoints binding");
		Router.route(financialreport);
		
		System.out.println("chartofaccount endpoints binding");
		Router.route(chartofaccount);
		
		System.out.println("activity endpoints binding");
		Router.route(activity);
		
		
		Router.route(userPassworded);
		Router.route(roleCore);
		System.out.println();
	}

}
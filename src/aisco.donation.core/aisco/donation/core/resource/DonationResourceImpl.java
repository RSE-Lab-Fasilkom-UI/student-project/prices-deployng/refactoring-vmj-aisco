package aisco.donation.core;

import java.util.*;

import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;

import aisco.donation.DonationFactory;

import aisco.program.core.*;
import aisco.chartofaccount.core.*;
import aisco.financialreport.core.*;
import aisco.financialreport.FinancialReportFactory;

import prices.auth.vmj.annotations.Restricted;

public class DonationResourceImpl extends DonationResourceComponent {
	
	private final int DONATION_COA_CODE = 42010;
	
	@Restricted(permissionName="ModifyDonationReportImpl")
    @Route(url="call/donation/save")
    public List<HashMap<String,Object>> saveDonation(VMJExchange vmjExchange) {
		if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;

        Donation donation = createDonation(vmjExchange);
        donationDao.saveObject(donation);
        System.out.println(donation);
        return getAllDonation(vmjExchange);
    }
	
	private FinancialReportComponent createIncome(VMJExchange vmjExchange) {
		String description = (String) vmjExchange.getRequestBodyForm("message");
		String paymentMethod = (String) vmjExchange.getRequestBodyForm("paymentmethod");
		String idProgramStr = (String) vmjExchange.getRequestBodyForm("idprogram");
		Program program = null;
        if (idProgramStr != null) {
            int idProgram = Integer.parseInt(idProgramStr);
            program = donationDao.getProxyObject(ProgramComponent.class, idProgram);
        }
		ChartOfAccountComponent coa = donationDao.getProxyObject(ChartOfAccountComponent.class, DONATION_COA_CODE);
        String date = (String) vmjExchange.getRequestBodyForm("transferdate");
        String amountStr = (String) vmjExchange.getRequestBodyForm("amount");
        long amount = Long.parseLong(amountStr);
		FinancialReport financialReport = FinancialReportFactory.createFinancialReport("aisco.financialreport.core.FinancialReportImpl", date, amount, description, program, coa);
		FinancialReport income = FinancialReportFactory.createFinancialReport("aisco.financialreport.income.FinancialReportImpl", financialReport, paymentMethod);
		return (FinancialReportComponent)income;
	}

    public Donation createDonation(VMJExchange vmjExchange) {
        return createDonation(vmjExchange, (new Random()).nextInt());
    }
    
    public Donation createDonation(VMJExchange vmjExchange, int id) {
		String name = (String) vmjExchange.getRequestBodyForm("name");
		String email = (String) vmjExchange.getRequestBodyForm("email");
		String phone = (String) vmjExchange.getRequestBodyForm("phonenumber");
		String description = (String) vmjExchange.getRequestBodyForm("message");
		String paymentMethod = (String) vmjExchange.getRequestBodyForm("paymentmethod");
		String idProgramStr = (String) vmjExchange.getRequestBodyForm("idprogram");
		ProgramComponent program = null;
        if (idProgramStr != null) {
            int idProgram = Integer.parseInt(idProgramStr);
            program = donationDao.getProxyObject(ProgramComponent.class, idProgram);
        }
        String date = (String) vmjExchange.getRequestBodyForm("transferdate");
        String amountStr = (String) vmjExchange.getRequestBodyForm("amount");
        long amount = Long.parseLong(amountStr);
        Donation donation = DonationFactory.createDonation("aisco.donation.core.DonationImpl", id, name, email, phone, amount, paymentMethod, date, program, description);
        Donation existingDonation = donationDao.getObject(id);
        FinancialReportComponent income;
        if (existingDonation == null) income = createIncome(vmjExchange);
        else income = existingDonation.getIncome();
        financialReportDao.saveObject(income);
        donation.setIncome(income);
        return donation;
    }
	
    @Restricted(permissionName="ModifyDonationReportImpl")
    @Route(url="call/donation/update")
    public HashMap<String, Object> updateDonation(VMJExchange vmjExchange) {
		if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;

        String idStr = (String) vmjExchange.getRequestBodyForm("id");
        int id = Integer.parseInt(idStr);
        Donation donation = createDonation(vmjExchange, id);
        donationDao.updateObject(donation);
        return donation.toHashMap();
    }

    @Route(url="call/donation/detail")
    public HashMap<String, Object> getDonation(VMJExchange vmjExchange) {
        String idStr = vmjExchange.getGETParam("id");
        int id = Integer.parseInt(idStr);
        Donation donation = donationDao.getObject(id);
        System.out.println(donation);
        try {
            return donation.toHashMap();
        } catch (NullPointerException e) {
            HashMap<String, Object> blank = new HashMap<>();
            blank.put("error", "Missing Params");
            return blank;
        }
    }

    @Route(url="call/donation/list")
    public List<HashMap<String,Object>> getAllDonation(VMJExchange vmjExchange) {
        List<Donation> donationList = donationDao.getAllObject("donation_impl");
        return transformDonationListToHashMap(donationList);
    }

    // TODO: bisa dimasukin ke kelas util
    public List<HashMap<String,Object>> transformDonationListToHashMap(List<Donation> donationList) {
        List<HashMap<String,Object>> resultList = new ArrayList<HashMap<String,Object>>();
        for(int i = 0; i < donationList.size(); i++) {
            resultList.add(donationList.get(i).toHashMap());
        }

        return resultList;
    }

    @Restricted(permissionName="ModifyDonationReportImpl")
    @Route(url="call/donation/delete")
    public List<HashMap<String,Object>> deleteDonation(VMJExchange vmjExchange) {
        String idStr = (String) vmjExchange.getRequestBodyForm("id");
        int id = Integer.parseInt(idStr);
        donationDao.deleteObject(id);
        return getAllDonation(vmjExchange);
    }
}

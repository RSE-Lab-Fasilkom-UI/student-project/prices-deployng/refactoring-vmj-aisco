package aisco.donation.confirmation;

import java.util.*;

import vmj.routing.route.Route;
import vmj.routing.route.VMJExchange;

import aisco.donation.DonationFactory;
import aisco.donation.core.DonationResourceDecorator;
import aisco.donation.core.DonationResourceComponent;
import aisco.donation.core.Donation;
import aisco.donation.core.DonationDecorator;

import aisco.donation.confirmation.validator.ProofOfTransferValidator;

import prices.auth.vmj.annotations.Restricted;

public class DonationResourceConfirmationDecorator extends DonationResourceDecorator {
	
	public DonationResourceConfirmationDecorator(DonationResourceComponent record) {
        super(record);
    }

	//@Restricted(permissionName="ModifyDonationReportImpl")
    @Route(url="call/dconfirm-offline-donation/save-full")
    public List<HashMap<String,Object>> saveDonation(VMJExchange vmjExchange) {
        Donation donation = createDonation(vmjExchange);
        donationDao.saveObject(donation);
        System.out.println(donation);
        return getAllDonation(vmjExchange);
    }
    
    @Route(url="call/confirm-offline-donation/save")
    public HashMap<String,Object> saveLiteDonation(VMJExchange vmjExchange) {
    	if (vmjExchange.getHttpMethod().equals("OPTIONS")) return null;
        Donation donation = createDonation(vmjExchange);
        donationDao.saveObject(donation);
        System.out.println(donation);
        return donation.toHashMap();
    }

    public Donation createDonation(VMJExchange vmjExchange) {
		String proofOfTransferUrl = (String) vmjExchange.getRequestBodyForm("proofoftransferurl");
//		String senderAccount = (String) vmjExchange.getRequestBodyForm("senderAccount");
//		String recieverAccount = (String) vmjExchange.getRequestBodyForm("recieverAccount");
		String senderAccount = "";
		String recieverAccount = "";
		Donation donation = record.createDonation(vmjExchange);
        Donation donationConfirmation = DonationFactory.createDonation("aisco.donation.confirmation.DonationConfirmationDecorator", donation,proofOfTransferUrl, senderAccount, recieverAccount);
        return donationConfirmation;
    }
    
    public Donation createDonation(VMJExchange vmjExchange, int id) {
		String proofOfTransferUrl = (String) vmjExchange.getRequestBodyForm("proofoftransferurl");
//		String senderAccount = (String) vmjExchange.getRequestBodyForm("senderAccount");
//		String recieverAccount = (String) vmjExchange.getRequestBodyForm("recieverAccount");
		String senderAccount = "";
		String recieverAccount = "";
		Donation savedDonation = donationDao.getObject(id);
        int recordId = (((DonationDecorator) savedDonation).getRecord()).getId();

		Donation donation = record.createDonation(vmjExchange, recordId);
        Donation donationConfirmation = DonationFactory.createDonation("aisco.donation.confirmation.DonationConfirmationDecorator", id, donation, proofOfTransferUrl, senderAccount, recieverAccount);
        return donationConfirmation;
    }
	
    //@Restricted(permissionName="ModifyDonationReportImpl")
    @Route(url="call/confirm-offline-donation/update")
    public HashMap<String, Object> updateDonation(VMJExchange vmjExchange) {
        String idStr = (String) vmjExchange.getRequestBodyForm("id");
        int id = Integer.parseInt(idStr);
        Donation donation = donationDao.getObject(id);
        donation = createDonation(vmjExchange, id);
        donationDao.updateObject(donation);
        return donation.toHashMap();
    }

    @Route(url="call/confirm-offline-donation/detail")
    public HashMap<String, Object> getDonation(VMJExchange vmjExchange) {
        return super.getDonation(vmjExchange);
    }

    @Route(url="call/confirm-offline-donation/list")
    public List<HashMap<String,Object>> getAllDonation(VMJExchange vmjExchange) {
        List<Donation> donationList = donationDao.getAllObject("donation_confirmation");
        return transformDonationListToHashMap(donationList);
    }

    // TODO: bisa dimasukin ke kelas util
    public List<HashMap<String,Object>> transformDonationListToHashMap(List<Donation> donationList) {
        List<HashMap<String,Object>> resultList = new ArrayList<HashMap<String,Object>>();
        for(int i = 0; i < donationList.size(); i++) {
            resultList.add(donationList.get(i).toHashMap());
        }

        return resultList;
    }

    //@Restricted(permissionName="ModifyDonationReportImpl")
    @Route(url="call/confirm-offline-donation/delete")
    public List<HashMap<String,Object>> deleteDonation(VMJExchange vmjExchange) {
        String idStr = (String) vmjExchange.getRequestBodyForm("id");
        int id = Integer.parseInt(idStr);
        donationDao.deleteObject(id);
        return getAllDonation(vmjExchange);
    }
}

package aisco.donation.confirmation;

import java.util.*;

import javax.persistence.Entity;
import javax.persistence.Table;

import aisco.donation.core.DonationDecorator;
import aisco.donation.core.DonationComponent;

@Entity(name="donation_confirmation")
@Table(name="donation_confirmation")
public class DonationConfirmationDecorator extends DonationDecorator {

	protected String proofOfTransferUrl;
	protected String senderAccount;
	protected String recieverAccount;
	
	public DonationConfirmationDecorator ()
    {
        super();
        this.proofOfTransferUrl = "";
		this.senderAccount = "";
		this.recieverAccount = "";
    }
	
	public DonationConfirmationDecorator(int id, DonationComponent record, String proofOfTransferUrl, String senderAccount, String recieverAccount) {
        super(id, record);
        this.proofOfTransferUrl = proofOfTransferUrl;
		this.senderAccount = senderAccount;
		this.recieverAccount = recieverAccount;
    }
	
	public DonationConfirmationDecorator(DonationComponent record, String proofOfTransferUrl, String senderAccount, String recieverAccount) {
		super(record);
		this.proofOfTransferUrl = proofOfTransferUrl;
		this.senderAccount = senderAccount;
		this.recieverAccount = recieverAccount;
    }
    
    public String getProofOfTransferUrl(){ return proofOfTransferUrl; }
    public void setProofOfTransferUrl(String proofOfTransferUrl){ this.proofOfTransferUrl = proofOfTransferUrl; }
	
	public String getSenderAccount(){ return senderAccount; }
    public void setSenderAccount(String senderAccount){ this.senderAccount = senderAccount; }
	
	public String getRecieverAccount(){ return recieverAccount; }
    public void setRecieverAccount(String recieverAccount){ this.recieverAccount = recieverAccount; }
    
    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> donationMap = record.toHashMap();
        donationMap.put("id", id);
        donationMap.put("proofOfTransferUrl", getProofOfTransferUrl());
		donationMap.put("senderAccoun", getSenderAccount());
		donationMap.put("recieverAccount", getRecieverAccount());
        return donationMap;
    }


}
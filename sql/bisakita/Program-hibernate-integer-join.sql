INSERT INTO program_comp (idprogram) VALUES(458134636);
INSERT INTO program_comp (idprogram) VALUES(1349531118);
INSERT INTO program_comp (idprogram) VALUES(1343279926);
INSERT INTO program_comp (idprogram) VALUES(1270189464);
INSERT INTO program_comp (idprogram) VALUES(1843226349);
INSERT INTO program_comp (idprogram) VALUES(467337084);
INSERT INTO program_comp (idprogram) VALUES(1336003443);


INSERT INTO program_impl (idprogram, name, description, target, partner, logoUrl, executionDate) VALUES(458134636,'Kita Belanja','Program Belanja dari Bisa Kita yang bertujuan untuk dapat membantu warga sekitar dalam mendapatkan sayur hidroponik dan daging berkualitas','Warga Cakung','BisaKita','https://images.freeimages.com/images/large-previews/13e/my-cat-1363423.jpg',NULL);
INSERT INTO program_impl (idprogram, name, description, target, partner, logoUrl, executionDate) VALUES(1349531118,'Makan Sehat','Makan Sehat adalah program untuk makanan sehat dan bergizi untuk santri raudhotul muhibbin',' Santri Raudhotul Muhibbin','Warga Jatinegara Baru','https://images.freeimages.com/images/large-previews/aff/winter-in-holland-1396288.jpg',NULL);
INSERT INTO program_impl (idprogram, name, description, target, partner, logoUrl, executionDate) VALUES(1343279926,'Khitanan','Khitanan adalah program dari DKM Masjid Al-Jabbar yang menggratiskan khitanan untuk 40 orang warga. Bekerjasama dengan dokter bedah dari Rumah Sakit Umum Koja. Sebelum proses khitanan terdapat games dan nonton film anak-anak muslim muslimah.',' Usia 4 - 12 tahun','Dokter RS Koja','https://images.freeimages.com/images/large-previews/03b/abstract-series-13-1529920.jpg',NULL);
INSERT INTO program_impl (idprogram, name, description, target, partner, logoUrl, executionDate) VALUES(1270189464,'Sumur Desa Makmur','Program untuk mendirikan sumur baru di desa makmur untuk warga desa','warga desa makmur','Bupati Rawabening','https://images.freeimages.com/images/large-previews/315/a-kayak-rider-passing-by-1641874.jpg',NULL);
INSERT INTO program_impl (idprogram, name, description, target, partner, logoUrl, executionDate) VALUES(1843226349,'Indonesia Melihat Jernih','Program periksa mata, penyediaan kacamata dan penyembuhan katarak untuk warga desa rawabakti', 'Warga rawa bakti', 'UNESCA','https://images.freeimages.com/images/large-previews/655/colorful-architecture-1-1216925.jpg',NULL);
INSERT INTO program_impl (idprogram, name, description, target, partner, logoUrl, executionDate) VALUES(467337084,'Posko Peduli','Program untuk menyediakan posko untuk warga luwu yang telah kehilangan rumah karena banjir bandang','warga luwu','Fondasi Peduli Bencana','https://images.freeimages.com/images/large-previews/d8a/perfect-blue-buildings-2-1235862.jpg',NULL);
INSERT INTO program_impl (idprogram, name, description, target, partner, logoUrl, executionDate) VALUES(1336003443,'Say No To Drugs','Program untuk menyingkirkan obat-obatan terlarang di daerah jatimangun sekaligus menyediakan pusat rehabilitasi untuk pecandu obat terlarang','warga jatimangun','Fondasi Walt','https://images.freeimages.com/images/large-previews/6ea/sting-ray-1197143.jpg',NULL);

INSERT INTO program_comp (idprogram) VALUES(458134636);
INSERT INTO program_comp (idprogram) VALUES(1349531118);
INSERT INTO program_comp (idprogram) VALUES(1343279926);
INSERT INTO program_comp (idprogram) VALUES(1270189464);


INSERT INTO program_impl (idprogram, name, description, target, partner, logoUrl, executionDate) VALUES(458134636,'Belanja Sembako','Program Belanja dari Pedulikita yang bertujuan untuk dapat membantu warga sekitar dalam mendapatkan sayur hidroponik dan daging berkualitas','Warga Cakung','Pedulikita','https://images.freeimages.com/images/large-previews/13e/my-cat-1363423.jpg',NULL);
INSERT INTO program_impl (idprogram, name, description, target, partner, logoUrl, executionDate) VALUES(1349531118,'Makanan Sehat Untuk Semua','Makanan Sehat Untuk Semua adalah program untuk makanan sehat dan bergizi untuk santri raudhotul muhibbin',' Santri Raudhotul Muhibbin','Warga Jatinegara Baru','https://images.freeimages.com/images/large-previews/aff/winter-in-holland-1396288.jpg',NULL);
INSERT INTO program_impl (idprogram, name, description, target, partner, logoUrl, executionDate) VALUES(1343279926,'Yuk kita Khitanan','Yuk kita Khitanan adalah program dari DKM Masjid Al-Jabbar yang menggratiskan khitanan untuk 40 orang warga. Bekerjasama dengan dokter bedah dari Rumah Sakit Umum Koja. Sebelum proses khitanan terdapat games dan nonton film anak-anak muslim muslimah.',' Usia 4 - 12 tahun','Dokter RS Koja','https://images.freeimages.com/images/large-previews/03b/abstract-series-13-1529920.jpg',NULL);
INSERT INTO program_impl (idprogram, name, description, target, partner, logoUrl, executionDate) VALUES(1270189464,'Sumber Air Desa Sukamadu','Program untuk mendirikan sumur baru di desa sukamadu untuk warga desa','warga desa sukamadu','Bupati Sukamadu','https://images.freeimages.com/images/large-previews/315/a-kayak-rider-passing-by-1641874.jpg',NULL);

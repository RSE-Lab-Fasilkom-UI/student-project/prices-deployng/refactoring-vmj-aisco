INSERT INTO public.financialreport_comp (id) VALUES (136797084);
INSERT INTO public.financialreport_comp (id) VALUES (1388483275);
INSERT INTO public.financialreport_comp (id) VALUES (1167247449);
INSERT INTO public.financialreport_comp (id) VALUES (1157936052);
INSERT INTO public.financialreport_comp (id) VALUES (1201953396);
INSERT INTO public.financialreport_comp (id) VALUES (1400048060);
INSERT INTO public.financialreport_comp (id) VALUES (1240456332);
INSERT INTO public.financialreport_comp (id) VALUES (1919840698);
INSERT INTO public.financialreport_comp (id) VALUES (2028214889);
INSERT INTO public.financialreport_comp (id) VALUES (1642167749);
INSERT INTO public.financialreport_comp (id) VALUES (1027445410);
INSERT INTO public.financialreport_comp (id) VALUES (1953009901);
INSERT INTO public.financialreport_comp (id) VALUES (832811356);
INSERT INTO public.financialreport_comp (id) VALUES (411412694);
INSERT INTO public.financialreport_comp (id) VALUES (6667367);
INSERT INTO public.financialreport_comp (id) VALUES (1559536658);
INSERT INTO public.financialreport_comp (id) VALUES (1400763756);
INSERT INTO public.financialreport_comp (id) VALUES (881540967);
INSERT INTO public.financialreport_comp (id) VALUES (1817062699);
INSERT INTO public.financialreport_comp (id) VALUES (783411393);
INSERT INTO public.financialreport_comp (id) VALUES (2143026892);
INSERT INTO public.financialreport_comp (id) VALUES (1260375617);
INSERT INTO public.financialreport_comp (id) VALUES (1744316060);
INSERT INTO public.financialreport_comp (id) VALUES (833283320);
INSERT INTO public.financialreport_comp (id) VALUES (1100084344);
INSERT INTO public.financialreport_comp (id) VALUES (1239680427);
INSERT INTO public.financialreport_comp (id) VALUES (1659899570);
INSERT INTO public.financialreport_comp (id) VALUES (1084147785);
INSERT INTO public.financialreport_comp (id) VALUES (1830411566);
INSERT INTO public.financialreport_comp (id) VALUES (1432990401);
INSERT INTO public.financialreport_comp (id) VALUES (1376415860);
INSERT INTO public.financialreport_comp (id) VALUES (558007821);
INSERT INTO public.financialreport_comp (id) VALUES (817722117);
INSERT INTO public.financialreport_comp (id) VALUES (1216370662);
INSERT INTO public.financialreport_comp (id) VALUES (2061382506);
INSERT INTO public.financialreport_comp (id) VALUES (1686291873);
INSERT INTO public.financialreport_comp (id) VALUES (1942018872);
INSERT INTO public.financialreport_comp (id) VALUES (332886715);
INSERT INTO public.financialreport_comp (id) VALUES (986294365);
INSERT INTO public.financialreport_comp (id) VALUES (879774870);


INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (5000000, '2021-09-05', 'Hasil penjualan Tupperware', 136797084, 43010, 458134636);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (5000000, '2021-09-05', 'Hasil penjualan ceasar salad', 1167247449, 43010, 1349531118);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (7000000, '2021-09-09', 'Hasil penjualan kaus kaki', 1201953396, 43010, 1343279926);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (3500000, '2021-09-03', 'Donasi walikota', 1240456332, 41010, 1343279926);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (2500000, '2021-09-03', 'Dana kantor sukamakmur', 2028214889, 41020, 1343279926);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (2200000, '2021-09-04', 'Garage sale panitia', 1027445410, 43010, 1343279926);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (10000000, '2021-09-05', 'Donasi hamba Allah', 832811356, 42020, 1343279926);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (25000000, '2021-09-05', 'Kas Umum daerah', 6667367, 12100, 1343279926);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (2000000, '2021-09-05', 'Biaya listrik', 1400763756, 66200, 1349531118);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (2000000, '2021-09-05', 'Cat tembok rutin', 1817062699, 66300, 1349531118);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (3000000, '2021-09-09', 'Konsumsi panitia', 2143026892, 65200, 1349531118);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (12000000, '2020-03-05', 'Donasi bupati rawabening', 1744316060, 41010, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (3000000, '2020-03-03', 'Donasi PT Hachim', 1100084344, 41020, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (1000000, '2020-03-05', 'Donasi Bapak Ahmad Shaleh', 1659899570, 42010, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (5000000, '2020-03-14', 'Hasil penjualan buah klengkeng Desa Makmur', 1830411566, 43010, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (15000000, '2020-03-14', 'Hasil penjualan buah durian Desa Makmur', 1376415860, 43010, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (1250000, '2020-04-05', 'Biaya publikasi ke sosial media', 817722117, 65100, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (4500000, '2020-04-03', 'Biaya konsumsi tukang bangunan', 2061382506, 65200, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (4000000, '2020-03-13', 'Biaya sewa peralatan bangunan', 1942018872, 65400, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (3500000, '2020-04-13', 'Biaya bensin dan token listrik', 986294365, 66200, 1270189464);



INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Bank Transfer', 1388483275, 136797084);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1157936052, 1167247449);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1400048060, 1201953396);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1919840698, 1240456332);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1642167749, 2028214889);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Bank Transfer', 1953009901, 1027445410);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 411412694, 832811356);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Cash', 1559536658, 6667367);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 833283320, 1744316060);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1239680427, 1100084344);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Bank Transfer', 1084147785, 1659899570);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Cash', 1432990401, 1830411566);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Cash', 558007821, 1376415860);



INSERT INTO public.financialreport_expense (id, record_id) VALUES (881540967, 1400763756);
INSERT INTO public.financialreport_expense (id, record_id) VALUES (783411393, 1817062699);
INSERT INTO public.financialreport_expense (id, record_id) VALUES (1260375617, 2143026892);
INSERT INTO public.financialreport_expense (id, record_id) VALUES (1216370662, 817722117);
INSERT INTO public.financialreport_expense (id, record_id) VALUES (1686291873, 2061382506);
INSERT INTO public.financialreport_expense (id, record_id) VALUES (332886715, 1942018872);
INSERT INTO public.financialreport_expense (id, record_id) VALUES (879774870, 986294365);
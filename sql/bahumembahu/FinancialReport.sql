INSERT INTO public.financialreport_comp (id) VALUES (136797084);
INSERT INTO public.financialreport_comp (id) VALUES (1388483275);
INSERT INTO public.financialreport_comp (id) VALUES (1167247449);
INSERT INTO public.financialreport_comp (id) VALUES (1157936052);
INSERT INTO public.financialreport_comp (id) VALUES (1201953396);
INSERT INTO public.financialreport_comp (id) VALUES (1400048060);
INSERT INTO public.financialreport_comp (id) VALUES (1240456332);
INSERT INTO public.financialreport_comp (id) VALUES (1919840698);
INSERT INTO public.financialreport_comp (id) VALUES (832811356);
INSERT INTO public.financialreport_comp (id) VALUES (411412694);
INSERT INTO public.financialreport_comp (id) VALUES (1744316060);
INSERT INTO public.financialreport_comp (id) VALUES (833283320);
INSERT INTO public.financialreport_comp (id) VALUES (1100084344);
INSERT INTO public.financialreport_comp (id) VALUES (1239680427);
INSERT INTO public.financialreport_comp (id) VALUES (1659899570);
INSERT INTO public.financialreport_comp (id) VALUES (1084147785);
INSERT INTO public.financialreport_comp (id) VALUES (1830411566);
INSERT INTO public.financialreport_comp (id) VALUES (1432990401);
INSERT INTO public.financialreport_comp (id) VALUES (1376415860);
INSERT INTO public.financialreport_comp (id) VALUES (558007821);
INSERT INTO public.financialreport_comp (id) VALUES (1131167598);
INSERT INTO public.financialreport_comp (id) VALUES (1229141793);
INSERT INTO public.financialreport_comp (id) VALUES (842971921);
INSERT INTO public.financialreport_comp (id) VALUES (275080470);
INSERT INTO public.financialreport_comp (id) VALUES (1614940375);
INSERT INTO public.financialreport_comp (id) VALUES (211374222);



INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (5000000, '2021-08-02', 'Hasil penjualan merchandise boneka', 136797084, 43010, 458134636);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (5000000, '2021-08-02', 'Hasil penjualan healthy snacks', 1167247449, 43010, 1349531118);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (7000000, '2021-08-06', 'Hasil penjualan celana jogger', 1201953396, 43010, 1343279926);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (3500000, '2021-08-06', 'Donasi walikota', 1240456332, 41010, 1343279926);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (40000000, '2021-08-04', 'Donasi hamba Allah', 832811356, 42020, 1343279926);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (12000000, '2020-08-02', 'Donasi kepala desa', 1744316060, 41010, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (5000000, '2020-08-08', 'Donasi PT Kakato Steel', 1100084344, 41020, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (4000000, '2020-08-04', 'Donasi Ibu Rani ', 1659899570, 42010, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (5000000, '2020-08-11', 'Hasil penjualan kerajinan tangan Desa Sidomungkur', 1830411566, 43010, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (15000000, '2020-08-11', 'Hasil penjualan hasil bumi Desa Sidomungkur', 1376415860, 43010, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (25000000, '2019-04-02', 'Donasi Klinik Mata KMO', 275080470, 41020, 1843226349);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (12500000, '2019-04-08', 'Donasi Gubernur Salatiga', 1614940375, 41010, 1843226349);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (40000000, '2019-04-04', 'Hasil penjualan kacamata 20 20', 211374222, 43010, 1843226349);




INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Bank Transfer', 1388483275, 136797084);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1157936052, 1167247449);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1400048060, 1201953396);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1919840698, 1240456332);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 411412694, 832811356);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 833283320, 1744316060);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1239680427, 1100084344);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Bank Transfer', 1084147785, 1659899570);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Cash', 1432990401, 1830411566);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Cash', 558007821, 1376415860);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Bank Transfer', 1131167598, 275080470);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1229141793, 1614940375);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Cash', 842971921, 211374222);




INSERT INTO public.financialreport_comp (id) VALUES (136797084);
INSERT INTO public.financialreport_comp (id) VALUES (1388483275);
INSERT INTO public.financialreport_comp (id) VALUES (1167247449);
INSERT INTO public.financialreport_comp (id) VALUES (1157936052);
INSERT INTO public.financialreport_comp (id) VALUES (1201953396);
INSERT INTO public.financialreport_comp (id) VALUES (1400048060);
INSERT INTO public.financialreport_comp (id) VALUES (1240456332);
INSERT INTO public.financialreport_comp (id) VALUES (1919840698);
INSERT INTO public.financialreport_comp (id) VALUES (2028214889);
INSERT INTO public.financialreport_comp (id) VALUES (1642167749);
INSERT INTO public.financialreport_comp (id) VALUES (1027445410);
INSERT INTO public.financialreport_comp (id) VALUES (1953009901);
INSERT INTO public.financialreport_comp (id) VALUES (832811356);
INSERT INTO public.financialreport_comp (id) VALUES (411412694);
INSERT INTO public.financialreport_comp (id) VALUES (6667367);
INSERT INTO public.financialreport_comp (id) VALUES (1559536658);
INSERT INTO public.financialreport_comp (id) VALUES (1744316060);
INSERT INTO public.financialreport_comp (id) VALUES (833283320);
INSERT INTO public.financialreport_comp (id) VALUES (1100084344);
INSERT INTO public.financialreport_comp (id) VALUES (1239680427);
INSERT INTO public.financialreport_comp (id) VALUES (1659899570);
INSERT INTO public.financialreport_comp (id) VALUES (1084147785);
INSERT INTO public.financialreport_comp (id) VALUES (1830411566);
INSERT INTO public.financialreport_comp (id) VALUES (1432990401);
INSERT INTO public.financialreport_comp (id) VALUES (1376415860);
INSERT INTO public.financialreport_comp (id) VALUES (558007821);
INSERT INTO public.financialreport_comp (id) VALUES (1131167598);
INSERT INTO public.financialreport_comp (id) VALUES (1229141793);
INSERT INTO public.financialreport_comp (id) VALUES (842971921);
INSERT INTO public.financialreport_comp (id) VALUES (275080470);
INSERT INTO public.financialreport_comp (id) VALUES (1614940375);
INSERT INTO public.financialreport_comp (id) VALUES (211374222);



INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (5000000, '2021-03-01', 'Hasil penjualan merchandise gelang', 136797084, 43010, 458134636);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (5000000, '2021-03-01', 'Hasil penjualan minuman beli 1 dapat 1 beri 1', 1167247449, 43010, 1349531118);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (7000000, '2021-03-03', 'Hasil penjualan merchandise comfypants', 1201953396, 43010, 1343279926);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (3500000, '2021-03-03', 'Donasi walikota', 1240456332, 41010, 1343279926);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (2500000, '2021-03-03', 'Dana kantor sukabagi', 2028214889, 41020, 1343279926);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (2200000, '2021-03-04', 'Garage sale panitia inti', 1027445410, 43010, 1343279926);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (10000000, '2021-03-07', 'Donasi hamba Allah', 832811356, 42020, 1343279926);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (25000000, '2021-03-07', 'Kas Umum daerah', 6667367, 12100, 1343279926);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (12000000, '2020-03-01', 'Donasi bupati rawabening', 1744316060, 41010, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (3000000, '2020-03-03', 'Donasi PT Hachim', 1100084344, 41020, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (1000000, '2020-03-07', 'Donasi Bapak Ahmad Shaleh', 1659899570, 42010, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (5000000, '2020-03-11', 'Hasil penjualan buah klengkeng Desa Makmur', 1830411566, 43010, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (15000000, '2020-03-11', 'Hasil penjualan buah durian Desa Makmur', 1376415860, 43010, 1270189464);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (25000000, '2019-07-01', 'Donasi PT Medieye', 275080470, 41020, 1843226349);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (12500000, '2019-07-03', 'Donasi Gubernur', 1614940375, 41010, 1843226349);
INSERT INTO public.financialreport_impl (amount, datestamp, description, id, coa_id, program_idprogram) VALUES (10000000, '2019-07-07', 'Hasil penjualan merchandise kaos', 211374222, 43010, 1843226349);




INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Bank Transfer', 1388483275, 136797084);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1157936052, 1167247449);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1400048060, 1201953396);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1919840698, 1240456332);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1642167749, 2028214889);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Bank Transfer', 1953009901, 1027445410);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 411412694, 832811356);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Cash', 1559536658, 6667367);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 833283320, 1744316060);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1239680427, 1100084344);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Bank Transfer', 1084147785, 1659899570);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Cash', 1432990401, 1830411566);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Cash', 558007821, 1376415860);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Bank Transfer', 1131167598, 275080470);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Check', 1229141793, 1614940375);
INSERT INTO public.financialreport_income (paymentmethod, id, record_id) VALUES ('Cash', 842971921, 211374222);



